import React from 'react';
import { Svg } from 'expo';
const { G, Path } = Svg;

const UserSvg = ({ scale = 1.2, color = "#FFFFFF", stroke = "none", strokeWidth = 1 }) => (
    <Svg
        width={16 * scale}
        height={16 * scale}>
        <G strokeWidth={strokeWidth} stroke={stroke} fill={color} fillRule="evenodd" scale={scale}>
            <Path fill={color} d="M8,0 C10.209139,0 12,1.790861 12,4 C12,6.209139 10.209139,8 8,8 C5.790861,8 4,6.209139 4,4 C4,1.790861 5.790861,0 8,0 L8,0 Z M8,10 C12.42,10 16,11.79 16,14 L16,16 L0,16 L0,14 C0,11.79 3.58,10 8,10 Z" />
        </G>
    </Svg>
);

export { UserSvg };