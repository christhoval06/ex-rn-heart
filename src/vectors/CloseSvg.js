import React from 'react';
import { Svg } from 'expo';
const { G, Polygon } = Svg;

const CloseSvg = ({ scale = 1.2, color = "#FFFFFF", stroke = "none", strokeWidth = 1 }) => (
    <Svg
        width={16 * scale}
        height={16 * scale}>
        <G strokeWidth={strokeWidth} stroke={stroke} fill={color} fillRule="evenodd" scale={scale}>
            <Polygon points="9.66857143 8 16 14.3314286 16 16 14.3314286 16 8 9.66857143 1.66857143 16 0 16 0 14.3314286 6.33142857 8 0 1.66857143 0 0 1.66857143 0 8 6.33142857 14.3314286 0 16 0 16 1.66857143" />
        </G>
    </Svg>
);

export { CloseSvg };