import React from 'react';
import { Constants, Svg } from 'expo';
const { G, Path } = Svg;

const DotsSvg = ({ scale = 1.2, color = "#FFFFFF", stroke = "none", strokeWidth = 1 }) => (
        <Svg
            width={24 * scale}
            height={6 * scale}>
            <G fill={color} strokeWidth={strokeWidth} stroke={stroke} fillRule="evenodd" scale={scale}>
                <Path d="M18,3 C18,1.34314575 19.3431458,0 21,0 C22.6568542,0 24,1.34314575 24,3 C24,4.65685425 22.6568542,6 21,6 C19.3431458,6 18,4.65685425 18,3 L18,3 Z M9,3 C9,1.34314575 10.3431458,0 12,0 C13.6568542,0 15,1.34314575 15,3 C15,4.65685425 13.6568542,6 12,6 C10.3431458,6 9,4.65685425 9,3 L9,3 Z M0,3 C0,1.34314575 1.34314575,0 3,0 C4.65685425,0 6,1.34314575 6,3 C6,4.65685425 4.65685425,6 3,6 C1.34314575,6 0,4.65685425 0,3 Z" />
            </G>
        </Svg>
);

export { DotsSvg };