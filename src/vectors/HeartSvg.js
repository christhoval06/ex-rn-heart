import React from 'react';
import { Constants, Svg } from 'expo';
const { G, Path } = Svg;

const HeartSvg = ({ scale = 1.2, color = "#FFFFFF", stroke = "none", strokeWidth = 1 }) => (
        <Svg
            width={20 * scale}
            height={19 * scale}>
            <G fill={color} strokeWidth={strokeWidth} stroke={stroke} fillRule="evenodd" scale={scale}>
                <Path d="M10,18.35 L8.55,17.03 C3.4,12.36 0,9.27 0,5.5 C0,2.41 2.42,0 5.5,0 C7.24,0 8.91,0.81 10,2.08 C11.09,0.81 12.76,0 14.5,0 C17.58,0 20,2.41 20,5.5 C20,9.27 16.6,12.36 11.45,17.03 L10,18.35 Z" />
            </G>
        </Svg>
);

export { HeartSvg };