import React from 'react'
import { StyleSheet, Image } from 'react-native'
import Main from './components/Main';

export default App = () => (
    <Image source={require('../assets/images/bg.png')} style={[styles.container, styles.bg]}>
        <Main startCount={0} />
    </Image>);

const styles = StyleSheet.create(
    {
        container: {
            flex: 1
        },
        bg: {
            width: null,
            height: null,
            resizeMode: 'cover'
        }
    }
);