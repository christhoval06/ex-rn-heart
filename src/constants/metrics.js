import { Dimensions, Platform } from 'react-native'

const IS_ANDROID = Platform.OS === 'android'
const { height, width } = Dimensions.get('window')
const MIN_TO_SHOW_ALL = 6
const APP_PADDING = 10

export default {
    ANDROID_STATUSBAR: 24,
    DEVICE_HEIGHT: IS_ANDROID ? height - 24 : height,
    DEVICE_WIDTH: width,
    MIN_TO_SHOW_ALL,
    APP_PADDING
}