export default getRandomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
}