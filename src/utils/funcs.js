export const getXYParticle = (total, i, radius) => {
    var angle = ((2 * Math.PI) / total) * i;
    var x = Math.round((radius * 2) * Math.cos(angle - (Math.PI / 2)));
    var y = Math.round((radius * 2) * Math.sin(angle - (Math.PI / 2)));
    return { x, y }
}
export const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
}

export const shuffle = (a) => {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
    return a;
}

export const getRandomIntInclusive = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const getRandomFromData = (data) => {
    return data[getRandomIntInclusive(0, data.length - 1)];
}

export const numberToByteScale = (number, decimals) => {
    if (number == 0) return 0;
    const k = 1e3,
        dm = decimals || 1,
        sizes = ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'],
        i = Math.floor(Math.log(number) / Math.log(k));
    return parseFloat((number / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
