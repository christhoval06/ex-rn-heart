import React from 'react'
import { StyleSheet, View, Text, Image, TextInput } from 'react-native'

import metrics from '../constants/metrics'

import { CloseSvg } from '../vectors'
import Button from './Button'

const Chat = ({ onCancelPress, style, ...props }) => (
    <View {...props} style={[styles.container, style]}>

        <View style={{ height: 1, backgroundColor: 'rgba(255,255,255,.4)' }} />

        <View style={{ flexDirection: 'row', paddingVertical: 10 }}>

            <View style={{ flex: .9, backgroundColor: 'white', borderRadius: 4, flexDirection: 'row' }}>
                <Image source={require('../../assets/images/user.jpg')} style={styles.user} />
                <TextInput placeholderTextColor={'#616B8C'} placeholder={'Di algo...'} underlineColorAndroid={'transparent'} style={styles.input} />
            </View>

            <Button icon={<CloseSvg scale={1} />} color={'transparent'} onPress={onCancelPress} style={{ flex: .1, paddingLeft: 5 }} />

        </View>
    </View>
);

const styles = StyleSheet.create(
    {
        container: {
            height: 48,
            paddingHorizontal: metrics.APP_PADDING,
            paddingVertical: 5,
            justifyContent: 'center'
        },
        user: {
            width: 36,
            height: 36,
            resizeMode: 'cover',
            borderTopLeftRadius: 4,
            borderBottomLeftRadius: 4
        },
        input: {
            flex: 1,
            padding: 5,
            color: '#616B8C',
            fontSize: 16
        }
    }
);
export default Chat;