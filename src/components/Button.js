import React from 'react'
import { StyleSheet, Platform, View, Text } from 'react-native'
import TouchableView from './TouchableView'

const Button = ({ text, icon, onPress = () => { }, color = 'rgba(0,0,0,.2)', style, ...props }) => (
    <View style={[styles.container, style]}>
        <TouchableView onPress={onPress} style={style}>
            <View {...props} style={[styles.content, { backgroundColor: color, alignItems: (icon ? 'center' : 'flex-start') }]}>
                {icon && icon}
                {text && <Text style={styles.textStyle}>{text}</Text>}
            </View>
        </TouchableView>
    </View>
);

const styles = StyleSheet.create(
    {
        container: {
            backgroundColor: 'transparent',
            flex: 1,
            height: 36,
            ...Platform.select({
                ios: {
                    shadowColor: 'black',
                    shadowOffset: { height: -3 },
                    shadowOpacity: 0.1,
                    shadowRadius: 3,
                },
                android: {
                    elevation: 3,
                },
            })
        },
        content: {
            flex: 1,
            justifyContent: 'center',
            borderRadius: 4,
            padding: 5
        },
        textStyle: {
            color: 'rgba(255,255,255,.4)',
            fontSize: 18,
            padding: 5
        }
    }
);
export default Button;