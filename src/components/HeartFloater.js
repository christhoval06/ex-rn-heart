import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native'
import AnimatedHeart from './AnimatedHeart'
import random from '../utils/random';

class HeartFloater extends Component {

    state = {
        hearts: [],
        startCount: 0
    }

    componentDidMount() {
        let { startCount } = this.props;
        this.setState({ startCount });
    }

    addHeart = () => {
        let { startCount, hearts } = this.state;
        startCount += 1;
        hearts.push({
            id: startCount,
            right: random(50, 150)
        });
        this.setState({ hearts, startCount });
    }

    removeHeart = (v) => {
        const { hearts } = this.state;
        var index = hearts.findIndex(function (heart) { return heart.id === v });
        hearts.splice(index, 1);
        this.setState(hearts);
    };

    render() {
        const { hearts } = this.state;
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback style={styles.container} onPress={this.addHeart}>
                    <View style={styles.container}>
                        {
                            hearts.map((v, i) => (<AnimatedHeart key={v.id} onComplete={() => { this.removeHeart(v.id) }} />))
                        }
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1
        },
    }
);

export default HeartFloater;