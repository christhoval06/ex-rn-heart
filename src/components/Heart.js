import React from 'react'
import { StyleSheet, View } from 'react-native'

const Heart = ({ style, ...props }) => (
    <View {...props} style={[styles.heart, style]}>
        <View style={[styles.heartShape, styles.leftHeart]} />
        <View style={[styles.heartShape, styles.rightHeart]} />
    </View>
);

const styles = StyleSheet.create(
    {
        heart: {
            width: 50,
            height: 50
        },
        heartShape: {
            width: 30,
            height: 45,
            position: 'absolute',
            top: 0,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'red',
        },
        leftHeart: {
            transform: [
                { rotate: '-45deg' }
            ],
            left: 5
        },
        rightHeart: {
            transform: [
                { rotate: '45deg' }
            ],
            right: 5
        }
    }
);
export default Heart;