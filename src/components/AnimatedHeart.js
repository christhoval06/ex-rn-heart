import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Animated, Dimensions } from 'react-native'

import metrics from '../constants/metrics'
import { getRandomFromData } from '../utils/funcs'

import { HeartSvg } from '../vectors'
import Heart from './Heart'


const ANIMATION_END_Y = Math.ceil(metrics.DEVICE_HEIGHT * .8);
const NEGATIVE_END_Y = ANIMATION_END_Y * -1;

const HEARTS_COLORS = [
    'rgb(158, 202, 250)',
    'rgb(161, 235, 206)',
    'rgb(208, 148, 246)',
    'rgb(244, 141, 166)',
    'rgb(234, 171, 104)',
    'rgb(170, 163, 186)'
];

class AnimatedHeart extends Component {
    static propTypes = {
        onComplete: PropTypes.func,
    }

    state = {
        position: new Animated.Value(0)
    }

    componentDidMount() {
        Animated.timing(this.state.position, {
            duration: 2000,
            toValue: NEGATIVE_END_Y
        }).start(this.props.onComplete);
    }

    componentWillMount() {
        this._yAnimation = this.state.position.interpolate({
            inputRange: [NEGATIVE_END_Y, 0],
            outputRange: [ANIMATION_END_Y, 0]
        });
        this._opacityAnimation = this._yAnimation.interpolate({
            inputRange: [0, ANIMATION_END_Y],
            outputRange: [1, 0]
        });
        this._scaleAnimation = this._yAnimation.interpolate({
            inputRange: [0, 15, 30],
            outputRange: [0, 1.2, 1],
            extrapolate: 'clamp'
        });
        this._xAnimation = this._yAnimation.interpolate({
            inputRange: [0, ANIMATION_END_Y / 2, ANIMATION_END_Y],
            outputRange: [0, 15, 0]
        });
        this._rotateAnimation = this._yAnimation.interpolate({
            inputRange: [0, ANIMATION_END_Y / 4, ANIMATION_END_Y / 3, ANIMATION_END_Y / 2, ANIMATION_END_Y],
            outputRange: ['0deg', '-2deg', '0deg', '2deg', '0deg']
        });
    }

    getHeartAnimationStyle = () => ({
        transform: [
            { translateY: this.state.position },
            { translateX: this._xAnimation },
            { scale: this._scaleAnimation },
            { rotate: this._rotateAnimation }
        ],
        opacity: this._opacityAnimation
    })

    render() {
        const { style } = this.props;
        return (
            <Animated.View style={[styles.heartWrap, this.getHeartAnimationStyle(), style]}>
                <HeartSvg scale={1.2} color={getRandomFromData(HEARTS_COLORS)} stroke="#fff" strokeWidth={.3} />
            </Animated.View>
        );
    }
}

AnimatedHeart.defaultProps = {
    onComplete: () => { }
}

const styles = StyleSheet.create(
    {
        heartWrap: {
            position: 'absolute',
            bottom: 50,
            right: (metrics.DEVICE_WIDTH * .2) - 25
        }
    }
);

export default AnimatedHeart;