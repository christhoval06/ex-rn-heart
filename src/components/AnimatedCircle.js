import React from 'react'
import { StyleSheet, View, Dimensions, TouchableWithoutFeedback, Animated } from 'react-native'
import ReactNativeART from 'ReactNativeART'

import metrics from '../constants/metrics'
import { getXYParticle, getRandomInt, shuffle } from '../utils/funcs'

const { Surface, Group, Shape, Path } = ReactNativeART;

const AnimatedShape = Animated.createAnimatedComponent(Shape);

const AnimatedCircle = ({ radius, ...props }) => {
    const path = Path().moveTo(0, -radius)
        .arc(0, radius * 2, radius)
        .arc(0, radius * -2, radius)
        .close();
    return (<AnimatedShape d={path} {...props} />)
}

export default AnimatedCircle;