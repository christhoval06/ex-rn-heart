import React, { Component } from 'react'
import { StyleSheet, KeyboardAvoidingView, View } from 'react-native'


import Header from './Header'
import Footer from './Footer'
import Chat from './Chat'

import AnimatedHeart from './AnimatedHeart'
import ExplodingHearts from './ExplodingHearts'

import random from '../utils/random'

class Main extends Component {
    state = {
        hearts: [],
        startCount: 0,
        users: 300,
        visibleView: null,
        showExplodeHeart: false
    }

    componentDidMount() {
        let { startCount } = this.props;
        this.setState({ startCount });
    }
    addHeart = () => {
        let { startCount, hearts, users } = this.state;
        startCount += 1;
        users += Math.ceil(random(0, 10));
        hearts.push({
            id: startCount,
            right: random(50, 150)
        });
        this.setState({ hearts, startCount, users });
    }

    removeHeart = (v) => {
        const { hearts } = this.state;
        var index = hearts.findIndex(function (heart) { return heart.id === v });
        hearts.splice(index, 1);
        this.setState(hearts);
    }

    _setVisibleForm = async (visibleView) => {
        this.setState({ visibleView });
    }

    render() {
        const { hearts, users, visibleView, showExplodeHeart } = this.state;
        const formStyle = (!visibleView) ? { height: 0 } : { marginTop: 40 }
        return (
            <View style={styles.container}>
                <Header />

                <View style={styles.container} />

                {
                    showExplodeHeart && (<ExplodingHearts explode={showExplodeHeart} onFinish={() => { this.setState({ showExplodeHeart: false }) }} />)
                }

                {
                    hearts.map((v, i) => (<AnimatedHeart style={{ right: v.right }} key={v.id} onComplete={() => { this.removeHeart(v.id) }} />))
                }

                {
                    !visibleView && (<Footer
                        onTextPress={() => this._setVisibleForm('CHAT')}
                        onHeartPress={this.addHeart} users={users}
                        onDootsPress={() => { this.setState({ showExplodeHeart: true }) }} />)
                }

                <KeyboardAvoidingView
                    keyboardVerticalOffset={-4}
                    behavior={'padding'}
                    style={[formStyle, styles.bottom]}>
                    {visibleView == 'CHAT' && (<Chat onCancelPress={() => this._setVisibleForm(null)} />)}
                </KeyboardAvoidingView>
            </View>
        );
    }
}

export default Main;

const styles = StyleSheet.create(
    {
        container: {
            flex: 1
        }
    }
);