import React, { Component } from 'react'
import { StyleSheet, View, Dimensions, TouchableWithoutFeedback, Animated } from 'react-native'
import ReactNativeART from 'ReactNativeART'

import metrics from '../constants/metrics'
import { getXYParticle, getRandomInt, shuffle } from '../utils/funcs'

import AnimatedCircle from './AnimatedCircle'

const { Surface, Group, Shape, Path } = ReactNativeART;

const AnimatedShape = Animated.createAnimatedComponent(Shape);

const HEART_SVG = "M130.4-0.8c25.4 0 46 20.6 46 46.1 0 13.1-5.5 24.9-14.2 33.3L88 153.6 12.5 77.3c-7.9-8.3-12.8-19.6-12.8-31.9 0-25.5 20.6-46.1 46-46.2 19.1 0 35.5 11.7 42.4 28.4C94.9 11 111.3-0.8 130.4-0.8"
const HEART_COLOR = 'rgb(226,38,77,1)';
const GRAY_HEART_COLOR = "rgb(204,204,204,1)";

const FILL_COLORS = [
    'rgba(221,70,136,1)',
    'rgba(212,106,191,1)',
    'rgba(204,142,245,1)',
    'rgba(204,142,245,1)',
    'rgba(204,142,245,1)',
    'rgba(0,0,0,0)'
];

const PARTICLE_COLORS = [
    'rgb(158, 202, 250)',
    'rgb(161, 235, 206)',
    'rgb(208, 148, 246)',
    'rgb(244, 141, 166)',
    'rgb(234, 171, 104)',
    'rgb(170, 163, 186)'
];

class ExplodingHearts extends Component {
    state = {
        animation: new Animated.Value(0)
    }

    componentDidMount() {
        this.explode()
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.explode) {
            this.explode();
        }
    }

    explode = () => {
        const { animation } = this.state;
        const { onFinish } = this.props;
        Animated.timing(animation, {
            duration: 1500,
            toValue: 28
        }).start(() => {
            this.state.animation.setValue(0);
            this.forceUpdate();
            onFinish();
        });
    }

    getSmallExplosions = (radius, offset) => {
        const { animation } = this.state;
        return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((v, i, t) => {
            var scaleOut = animation.interpolate({
                inputRange: [0, 5.99, 6, 13.99, 14, 21],
                outputRange: [0, 0, 1, 1, 1, 0],
                extrapolate: 'clamp'
            });
            var moveUp = animation.interpolate({
                inputRange: [0, 5.99, 14],
                outputRange: [0, 0, -15],
                extrapolate: 'clamp'
            });
            var moveDown = animation.interpolate({
                inputRange: [0, 5.99, 14],
                outputRange: [0, 0, 15],
                extrapolate: 'clamp'
            });
            var color_top_particle = animation.interpolate({
                inputRange: [6, 8, 10, 12, 17, 21],
                outputRange: shuffle(PARTICLE_COLORS)
            })
            var color_bottom_particle = animation.interpolate({
                inputRange: [6, 8, 10, 12, 17, 21],
                outputRange: shuffle(PARTICLE_COLORS)
            })
            var position = getXYParticle(10, i, radius)
            return (
                <Group key={i}
                    x={position.x + offset.x}
                    y={position.y + offset.y}
                    rotation={getRandomInt(0, 40) * i}>
                    <AnimatedCircle
                        x={moveUp}
                        y={moveUp}
                        radius={15}
                        scale={scaleOut}
                        fill={color_top_particle} />
                    <AnimatedCircle
                        x={moveDown}
                        y={moveDown}
                        radius={8}
                        scale={scaleOut}
                        fill={color_bottom_particle} />
                </Group>
            )
        }, this)
    }

    componentWillMount() {
        const { animation } = this.state;
        this.heart_scale = animation.interpolate({
            inputRange: [0, .01, 6, 10, 12, 18, 28],
            outputRange: [0, .1, .2, 1, 1.2, 1, 1],
            extrapolate: 'clamp'
        });
        this.heart_fill = animation.interpolate({
            inputRange: [0, 2],
            outputRange: [GRAY_HEART_COLOR, HEART_COLOR],
            extrapolate: 'clamp'
        })
        this.heart_x = this.heart_scale.interpolate({
            inputRange: [0, 1],
            outputRange: [90, 0],
        })
        this.heart_y = this.heart_scale.interpolate({
            inputRange: [0, 1],
            outputRange: [75, 0],
        })

        this.circle_scale = animation.interpolate({
            inputRange: [0, 1, 4],
            outputRange: [0, .3, 1],
            extrapolate: 'clamp'
        });
        this.circle_stroke_width = animation.interpolate({
            inputRange: [0, 5.99, 6, 7, 10],
            outputRange: [0, 0, 15, 8, 0],
            extrapolate: 'clamp'
        });
        this.circle_fill_colors = animation.interpolate({
            inputRange: [1, 2, 3, 4, 4.99, 5],
            outputRange: FILL_COLORS,
            extrapolate: 'clamp'
        })
        this.circle_opacity = animation.interpolate({
            inputRange: [1, 9.99, 10],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp'
        })
    }

    render() {
        const { animation } = this.state;
        return (
            <View style={styles.container}>
                <Surface width={metrics.DEVICE_WIDTH} height={metrics.DEVICE_HEIGHT}>
                    <Group x={metrics.DEVICE_WIDTH * .5 - 100} y={metrics.DEVICE_HEIGHT * .5 - 50}>
                        <AnimatedShape
                            d={HEART_SVG}
                            x={this.heart_x}
                            y={this.heart_y}
                            scale={this.heart_scale}
                            fill={this.heart_fill} />

                        <AnimatedCircle
                            x={89}
                            y={75}
                            radius={120}
                            scale={this.circle_scale}
                            strokeWidth={this.circle_stroke_width}
                            stroke={FILL_COLORS[2]}
                            fill={this.circle_fill_colors}
                            opacity={this.circle_opacity} />

                        {this.getSmallExplosions(70, { x: 90, y: 75 })}
                    </Group>

                </Surface>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            left: 0
        }
    }
);

export default ExplodingHearts;