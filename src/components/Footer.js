import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

import metrics from '../constants/metrics'
import { numberToByteScale } from '../utils/funcs'

import { HeartSvg, DotsSvg, UserSvg } from '../vectors'
import Button from './Button'

const Footer = ({ users = 0, onTextPress = () => { }, onHeartPress = () => { }, onDootsPress = () => { }, style, ...props }) => (
    <View {...props} style={[styles.container, style]}>

        <View style={{ height: 1, backgroundColor: 'rgba(255,255,255,.4)' }} />

        <View style={{ flexDirection: 'row', paddingVertical: 10 }}>

            <Button text={'Di algo...'} style={{ flex: .45 }} onPress={onTextPress} />

            <Button icon={<HeartSvg scale={1} />} onPress={onHeartPress} style={{ flex: .17, paddingLeft: 5 }} />

            <Button icon={<DotsSvg scale={1} />} onPress={onDootsPress} style={{ flex: .17, paddingLeft: 5 }} />

            <Button style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} color={'transparent'} icon={
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <UserSvg scale={1} />
                    <Text style={{ color: 'white', fontSize: 18, padding: 5, fontWeight: '400' }}>{numberToByteScale(users)}</Text>
                </View>
            } />

        </View>
    </View>
);

const styles = StyleSheet.create(
    {
        container: {
            height: 48,
            paddingHorizontal: metrics.APP_PADDING,
            paddingVertical: 5,
            justifyContent: 'center'
        }
    }
);
export default Footer;