import React from 'react'
import Expo from 'expo'
import { StyleSheet, View } from 'react-native'
import metrics from '../constants/metrics'
import { CloseSvg } from '../vectors'

const Header = ({ style, ...props }) => (
    <Expo.LinearGradient
        {...props}
        colors={['#000000', 'rgba(255,255,255,0)']} style={[{ height: 88, paddingHorizontal: metrics.APP_PADDING, justifyContent: 'center' }, style]}>
        <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: .9 }}></View>
            <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center' }}>
                <CloseSvg scale={1} />
            </View>
        </View>
    </Expo.LinearGradient>
);

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 36,
            backgroundColor: 'rgba(255,255,255,.5)',
            borderRadius: 4
        }
    }
);
export default Header;